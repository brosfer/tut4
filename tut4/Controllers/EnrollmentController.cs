﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tut4.DTOs.Request;
using Microsoft.AspNetCore.Mvc;
using tut4.Services;
using tut4.DTOs;
using System.Data.SqlClient;
using tut4.Models;

namespace tut4.Controllers
{
    [Route("api/enrollment")]
    [ApiController]
    public class EnrollmentsController : ControllerBase
    {
        private IStudentServiceDb _service;


        [Route("/api/enrollment")]
        [ApiController]
        class EnrollmentController : ControllerBase
        {
            private IStudentsDbService _service;
            public EnrollmentController(IStudentsDbService service)
            {
                this._service = service;
            }
            
            [HttpPost(Name = nameof(EnrollStudent))]
            [Route("enroll")]
            public IActionResult EnrollStudent(Student s)
            {

                var result = _service.EnrollStudent;
                if (result != null) return CreatedAtAction(nameof(EnrollStudent), result);
                return BadRequest();

            }

            private IActionResult BadRequest(object ındexNumber)
            {
                throw new NotImplementedException();
            }

            [HttpPost(Name = nameof(Promote))]
            [Route("promote")]
            public IActionResult Promote(Enrollment e)
            {
                var result = _service.PromoteStudent(e);
                if (result != null) return CreatedAtAction(nameof(Promote), result);
                return BadRequest(result);
            }




        }


    }
}