﻿using tut4.Models;

namespace tut4.Controllers
{
    internal interface IStudentsDbService
    {
        object EnrollStudent { get; }

        object PromoteStudent(Enrollment e);
    }
}