﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tut4.DTOs.Request;

namespace tut4.Services
{
   public interface IStudentServiceDb
    {
        void EnrollStudent(EnrollRequest request);
        void PromoteStudents(int semester, int idStudy);
    }
}
